/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.exposer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.Collections;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.content.impl.FileContentManager;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.rdf.Value;
import org.ow2.weblab.service.exposer.config.RestServiceConfig;
import org.ow2.weblab.service.exposer.enricher.RestServiceExposer;
import org.ow2.weblab.service.exposer.enricher.WebserverContextExposer;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.purl.dc.terms.DCTermsAnnotator;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 */
public class TestExposer {


	/**
	 *
	 */
	private static final URI TEST = URI.create("urn:test");


	@Test
	@Deprecated
	public void testDeprecatedExposer() throws Exception {
		final org.ow2.weblab.service.exposer.config.ExposerConfigBean conf = new org.ow2.weblab.service.exposer.config.ExposerConfigBean();
		conf.populateWithDefaults();
		conf.setUrlEncodeBetweenSlashes(true);

		final LocalFileExposer exp = new LocalFileExposer(conf);

		final Document doc = WebLabResourceFactory.createResource("azerty", "testDeprecatedExposer", Document.class);
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeSource("D:\\hjd# fhdf\\gsjn°f df\\fh hf\\fd fd+gg.doc");

		final ProcessArgs args = new ProcessArgs();
		args.setResource(doc);
		exp.process(args);
		new WebLabMarshaller(true).marshalResource(doc, new File("target/testDeprecatedExposer.xml"));

		final Value<String> exposedAs = new WProcessingAnnotator(doc).readExposedAs();
		Assert.assertTrue(exposedAs.hasValue());
		Assert.assertEquals(1, exposedAs.size());
		Assert.assertFalse(exposedAs.firstTypedValue().isEmpty());
		Assert.assertFalse(exposedAs.firstTypedValue().contains("°"));
		Assert.assertFalse(exposedAs.firstTypedValue().contains("#"));
		Assert.assertFalse(exposedAs.firstTypedValue().contains("+"));
		Assert.assertFalse(exposedAs.firstTypedValue().contains("\\"));
	}


	@Test
	public void testWebserverExposer() throws Exception {
		final CustomFileExposer exp = new CustomFileExposer(new WebserverContextExposer());

		final Document doc = WebLabResourceFactory.createResource("azerty", "testWebserverExposer", Document.class);
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeSource("D:\\hjd# fhdf\\gsjn°f df\\fh hf\\fd fd+gg.doc");

		final ProcessArgs args = new ProcessArgs();
		args.setResource(doc);
		exp.process(args);
		new WebLabMarshaller(true).marshalResource(doc, new File("target/testWebserverExposer.xml"));

		final Value<String> exposedAs = new WProcessingAnnotator(doc).readExposedAs();
		Assert.assertTrue(exposedAs.hasValue());
		Assert.assertEquals(1, exposedAs.size());
		Assert.assertFalse(exposedAs.firstTypedValue().contains("°"));
		Assert.assertFalse(exposedAs.firstTypedValue().contains("#"));
		Assert.assertFalse(exposedAs.firstTypedValue().contains("+"));
		Assert.assertFalse(exposedAs.firstTypedValue().contains("\\"));
	}


	@Test
	public void testRestExposer() throws Exception {
		final RestServiceConfig conf = new RestServiceConfig();
		conf.setExposeSubMediaUnits(true);
		conf.setEncoding("UTF-8");
		conf.setFormatParameter("theFormat");
		conf.setFileParameter("theFile");
		conf.setPrefix("thePrefix");
		conf.setExposeThumbnails(true);
		final CustomFileExposer exp = new CustomFileExposer(new RestServiceExposer(conf));

		final Document doc = WebLabResourceFactory.createResource("azerty", "testRestExposer1", Document.class);
		final Image subMediaUnit = WebLabResourceFactory.createAndLinkMediaUnit(doc, Image.class);
		final WProcessingAnnotator docWPA = new WProcessingAnnotator(doc);
		final URI contentUri;
		final WProcessingAnnotator subWPA = new WProcessingAnnotator(subMediaUnit);
		try (final FileInputStream fis = new FileInputStream("pom.xml")) {
			contentUri = ContentManager.getInstance().create(fis);
			docWPA.writeNativeContent(contentUri);
			docWPA.writeNormalisedContent(TestExposer.TEST);
			docWPA.startInnerAnnotatorOn(contentUri);
			docWPA.writeType(RestServiceExposer.THUMBNAIL);
			docWPA.endInnerAnnotator();
			new DCTermsAnnotator(doc).writeFormatOf("wonderful/mime");
			new DublinCoreAnnotator(doc).writeFormat("other/mime");
			subWPA.writeNormalisedContent(contentUri);
			new DublinCoreAnnotator(subMediaUnit).writeFormat("other/mime");
		}

		final ProcessArgs args = new ProcessArgs();
		args.setResource(doc);
		exp.process(args);
		new WebLabMarshaller(true).marshalResource(doc, new File("target/testRestExposer1.xml"));

		final Value<String> docExposedAs = docWPA.readExposedAs();
		Assert.assertTrue(docExposedAs.hasValue());
		Assert.assertEquals(1, docExposedAs.size());

		final Value<String> docExposedAsThumbnail = docWPA.readExposedAsThumbnail();
		Assert.assertTrue(docExposedAsThumbnail.hasValue());
		Assert.assertEquals(1, docExposedAsThumbnail.size());

		Assert.assertFalse(docExposedAs.firstTypedValue().contains("pom"));
		Assert.assertFalse(docExposedAs.firstTypedValue().contains("xml"));
		Assert.assertTrue(docExposedAs.firstTypedValue().contains("content"));
		Assert.assertFalse(docExposedAs.firstTypedValue().contains("format"));
		Assert.assertFalse(docExposedAs.firstTypedValue().contains("content-exposer"));
		Assert.assertTrue(docExposedAs.firstTypedValue().contains("theFormat"));
		Assert.assertTrue(docExposedAs.firstTypedValue().endsWith("wonderful%2Fmime"));
		Assert.assertTrue(docExposedAs.firstTypedValue().contains("thePrefix"));
		Assert.assertFalse(docExposedAs.firstTypedValue().contains("file"));
		Assert.assertTrue(docExposedAs.firstTypedValue().contains("theFile"));
		Assert.assertEquals(docExposedAs.firstTypedValue(), docExposedAsThumbnail.firstTypedValue());

		docWPA.startInnerAnnotatorOn(contentUri);
		Assert.assertTrue(docWPA.readExposedAs().hasValue());

		docWPA.startInnerAnnotatorOn(TestExposer.TEST);
		Assert.assertTrue(docWPA.readExposedAs().hasValue());


		final Value<String> imgExposedAs = subWPA.readExposedAs();
		Assert.assertTrue(imgExposedAs.hasValue());
		Assert.assertEquals(1, imgExposedAs.size());

		Assert.assertFalse(imgExposedAs.firstTypedValue().contains("pom"));
		Assert.assertFalse(imgExposedAs.firstTypedValue().contains("xml"));
		Assert.assertTrue(imgExposedAs.firstTypedValue().contains("content"));
		Assert.assertTrue(imgExposedAs.firstTypedValue().contains("theFormat"));
		Assert.assertTrue(imgExposedAs.firstTypedValue().endsWith("other%2Fmime"));
		Assert.assertTrue(imgExposedAs.firstTypedValue().contains("thePrefix"));
		Assert.assertFalse(imgExposedAs.firstTypedValue().contains("file"));
		Assert.assertTrue(imgExposedAs.firstTypedValue().contains("theFile"));

		subWPA.startInnerAnnotatorOn(contentUri);
		Assert.assertEquals(1, subWPA.readExposedAs().size());

		conf.setExposeThumbnails(false);
		final Document doc2 = WebLabResourceFactory.createResource("azerty", "testRestExposer2", Document.class);
		final Image subMediaUnit2 = WebLabResourceFactory.createAndLinkMediaUnit(doc2, Image.class);
		final URI contentUri2;
		final WProcessingAnnotator subWPA2 = new WProcessingAnnotator(subMediaUnit2);
		try (final FileInputStream fis = new FileInputStream("pom.xml")) {
			contentUri2 = ContentManager.getInstance().create(fis);
			subWPA2.writeNativeContent(contentUri2);
			subWPA2.startInnerAnnotatorOn(contentUri);
			subWPA2.writeType(RestServiceExposer.THUMBNAIL);
			subWPA2.endInnerAnnotator();
		}

		args.setResource(doc2);
		exp.process(args);
		new WebLabMarshaller(true).marshalResource(doc2, new File("target/testRestExposer2.xml"));

		Assert.assertFalse(new WProcessingAnnotator(doc2).readExposedAs().hasValue());

		final Value<String> imgExposedAs2 = subWPA2.readExposedAs();
		Assert.assertTrue(imgExposedAs2.hasValue());
		Assert.assertEquals(1, imgExposedAs2.size());

		Assert.assertFalse(imgExposedAs2.firstTypedValue().contains("pom"));
		Assert.assertFalse(imgExposedAs2.firstTypedValue().contains("xml"));
		Assert.assertTrue(imgExposedAs2.firstTypedValue().endsWith("content"));

		Assert.assertFalse(subWPA2.readExposedAsThumbnail().hasValue());

		subWPA2.startInnerAnnotatorOn(contentUri2);
		Assert.assertEquals(1, subWPA2.readExposedAs().size());
	}


	@Test
	public void testCxfConfiguredExposer() throws Exception {
		// Load the configuration inside the cxf bean directly
		final CustomFileExposer exp;
		try (FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/main/webapp/WEB-INF/applicationContext.xml")) {
			exp = fsxac.getBean("exposer", CustomFileExposer.class);
		}

		final Document doc = WebLabResourceFactory.createResource("azerty", "testCxfConfiguredExposer1", Document.class);
		final Image subMediaUnit = WebLabResourceFactory.createAndLinkMediaUnit(doc, Image.class);
		try (FileInputStream fis = new FileInputStream("pom.xml")) {
			new WProcessingAnnotator(subMediaUnit).writeNativeContent(ContentManager.getInstance().create(fis));
		}
		new DublinCoreAnnotator(doc).writeSource("D:\\hjd# fhdf\\gsjn°f df\\fh hf\\fd fd+gg.doc");

		final ProcessArgs args = new ProcessArgs();
		args.setResource(doc);
		exp.process(args);
		new WebLabMarshaller(true).marshalResource(doc, new File("target/testCxfConfiguredExposer1.xml"));

		Assert.assertFalse(new WProcessingAnnotator(doc).readExposedAs().hasValue());
		Assert.assertFalse(new WProcessingAnnotator(subMediaUnit).readExposedAs().hasValue());


		final Document doc2 = WebLabResourceFactory.createResource("azerty", "testCxfConfiguredExposer2", Document.class);
		final Image subMediaUnit2 = WebLabResourceFactory.createAndLinkMediaUnit(doc2, Image.class);
		try (FileInputStream fis = new FileInputStream("pom.xml")) {
			final URI contentUri = ContentManager.getInstance().create(fis);
			new WProcessingAnnotator(doc2).writeNativeContent(contentUri);
			new WProcessingAnnotator(subMediaUnit2).writeNativeContent(contentUri);
		}
		new DublinCoreAnnotator(doc2).writeSource("D:\\hjd# fhdf\\gsjn°f df\\fh hf\\fd fd+gg.doc");

		args.setResource(doc2);
		exp.process(args);
		new WebLabMarshaller(true).marshalResource(doc2, new File("target/testCxfConfiguredExposer2.xml"));

		Assert.assertTrue(new WProcessingAnnotator(doc2).readExposedAs().hasValue());
		Assert.assertFalse(new WProcessingAnnotator(subMediaUnit2).readExposedAs().hasValue());


		final Document doc3 = WebLabResourceFactory.createResource("azerty", "testCxfConfiguredExposer3", Document.class);
		final File targetFile = new File("target/content/testCxfConfiguredExposer3.xml");
		new DublinCoreAnnotator(doc2).writeSource(targetFile.getAbsolutePath());
		final Image subMediaUnit3 = WebLabResourceFactory.createAndLinkMediaUnit(doc3, Image.class);
		final URI contentUri;
		try (final FileInputStream fis = new FileInputStream("pom.xml"); final FileOutputStream fos = FileUtils.openOutputStream(targetFile)) {
			IOUtils.copy(fis, fos);
			contentUri = ContentManager.getInstance().create(fis, doc3, Collections.singletonMap(FileContentManager.LOCAL_CONTENT, (Object) Boolean.TRUE));
		}
		new WProcessingAnnotator(doc3).writeNativeContent(contentUri);
		new WProcessingAnnotator(subMediaUnit3).writeNativeContent(contentUri);

		args.setResource(doc3);
		exp.process(args);
		new WebLabMarshaller(true).marshalResource(doc3, new File("target/testCxfConfiguredExposer3.xml"));

		Assert.assertTrue(new WProcessingAnnotator(doc3).readExposedAs().hasValue());
		Assert.assertFalse(new WProcessingAnnotator(subMediaUnit3).readExposedAs().hasValue());


		final Document doc4 = WebLabResourceFactory.createResource("azerty", "testCxfConfiguredExposer4", Document.class);
		final URI relativeContentUri = URI.create("file:localUrl/with/subfolders.test");
		new WProcessingAnnotator(doc4).writeNativeContent(relativeContentUri);

		args.setResource(doc4);
		exp.process(args);
		new WebLabMarshaller(true).marshalResource(doc4, new File("target/testCxfConfiguredExposer4.xml"));

		Assert.assertTrue(new WProcessingAnnotator(doc4).readExposedAs().hasValue());
		Assert.assertEquals(new WProcessingAnnotator(doc4).readExposedAs().firstTypedValue(), "/content-exposer?file=localUrl%2Fwith%2Fsubfolders.test");


		final Document doc5 = WebLabResourceFactory.createResource("azerty", "testCxfConfiguredExposer5", Document.class);
		final URI absoluteContentUri = new File("pom.xml").getAbsoluteFile().toURI();
		new WProcessingAnnotator(doc5).writeNativeContent(absoluteContentUri);

		args.setResource(doc5);
		exp.process(args);
		new WebLabMarshaller(true).marshalResource(doc5, new File("target/testCxfConfiguredExposer5.xml"));

		Assert.assertTrue(new WProcessingAnnotator(doc5).readExposedAs().hasValue());
		Assert.assertEquals(new WProcessingAnnotator(doc5).readExposedAs().firstTypedValue(), "/content-exposer?file=pom.xml");


		final Document doc6 = WebLabResourceFactory.createResource("azerty", "testCxfConfiguredExposer6", Document.class);
		final URI isThisAnUri = URI.create("this_is_not_an_absolute_URI");
		new WProcessingAnnotator(doc6).writeNativeContent(isThisAnUri);

		args.setResource(doc6);
		try {
			exp.process(args);
			Assert.fail("Non absolute URI are not handled.");
		} catch (final UnexpectedException e) {
			Assert.assertNotNull(e);
		}

		try {
			exp.process(null);
			Assert.fail("No process args");
		} catch (final InvalidParameterException e) {
			Assert.assertNotNull(e);
		}

		try {
			exp.process(new ProcessArgs());
			Assert.fail("No resource");
		} catch (final InvalidParameterException e) {
			Assert.assertNotNull(e);
		}

		args.setResource(null);
		try {
			exp.process(args);
			Assert.fail("No resource");
		} catch (final InvalidParameterException e) {
			Assert.assertNotNull(e);
		}

		args.setResource(WebLabResourceFactory.createResource("toto", "tutu", Image.class));
		try {
			exp.process(args);
			Assert.fail("Not a document");
		} catch (final InvalidParameterException e) {
			Assert.assertNotNull(e);
		}
	}





}
