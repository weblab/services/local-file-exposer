/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.exposer;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.service.exposer.constants.Constants;
import org.ow2.weblab.service.exposer.enricher.DocumentEnricher;
import org.purl.dc.elements.DublinCoreAnnotator;


/**
 * This class is in charge of enriching the received {@link Resource} in order to add information on how it can be accessed online by the end user (often by adding an
 * {@link WebLabProcessing#IS_EXPOSED_AS} property).
 *
 * The difference with previous version (LocalFileExposer) is that it enables the customisation of way input source is read and how the transformation is done.
 *
 * @author ymombrun
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class CustomFileExposer implements Analyser {


	private final Log logger;


	private final DocumentEnricher enricher;


	/**
	 * @param enricher
	 *            The actual class in charge of the transformation.
	 */
	public CustomFileExposer(final DocumentEnricher enricher) {
		this.enricher = enricher;
		this.logger = LogFactory.getLog(this.getClass());
		this.logger.info("<"+Constants.SERVICE_NAME+">"+" successfully started using " + enricher.getClass().getName() + " as transformer.");
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws UnexpectedException, InvalidParameterException, ContentNotAvailableException, UnsupportedRequestException {
		final Document doc = this.checkArgs(args);
		
		final String resourceUri = doc.getUri();
		final String source = new DublinCoreAnnotator(doc).readSource().firstTypedValue();
		this.logger.debug("'<"+Constants.SERVICE_NAME+">'" + " Start processing document " + resourceUri+ " - " + "'<"+source+">'");
		this.enricher.enrichDocument(doc);
		this.logger.debug("'<"+Constants.SERVICE_NAME+">'" + " End processing document " + resourceUri+ " - " + "'<"+source+">'");
		final ProcessReturn pr = new ProcessReturn();
		pr.setResource(doc);
		return pr;

	}


	/**
	 * @param args
	 *            The ProcessArgs to check validity
	 * @return The document that must contain the resource in ProcessArgs
	 */
	private Document checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String message = "ProcessArgs is null";
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'" + " Unable to properly process document "+message);
		}
		final Resource res = args.getResource();
		if (res == null) {
			final String message = "Resource is null";
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'" + " Unable to properly process document "+message);
		}
		if (!(res instanceof Document)) {
			final String message = "Resource was not a Document but a: " + args.getResource().getClass().getName() + ".";
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'" + " Unable to properly process document "+message);
		}
		return (Document) args.getResource();
	}

}
