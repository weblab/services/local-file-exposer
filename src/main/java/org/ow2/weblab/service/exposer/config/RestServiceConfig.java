/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.exposer.config;



/**
 * The configuration holder for Rest Service exposer.
 *
 * @author ymombrun
 */
public class RestServiceConfig {


	private String prefix;


	private String fileParameter;


	private String formatParameter;


	private String encoding;


	/**
	 * Whether or not to add isExposedAs annotation to sub media units.
	 */
	private boolean exposeSubMediaUnits;


	private boolean exposeThumbnails;


	/**
	 * Set default values for all the fields on the class;
	 */
	public void populateWithDefaults() {
		if (this.encoding == null) {
			this.encoding = "ISO-8859-1";
		}
		if (this.prefix == null) {
			this.prefix = "/content-exposer";
		}
		if (this.fileParameter == null) {
			this.fileParameter = "file";
		}
		if (this.formatParameter == null) {
			this.formatParameter = "format";
		}
	}


	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return this.prefix;
	}


	/**
	 * @param prefix
	 *            the prefix to set
	 */
	public void setPrefix(final String prefix) {
		this.prefix = prefix;
	}


	/**
	 * @return the fileParameter
	 */
	public String getFileParameter() {
		return this.fileParameter;
	}


	/**
	 * @param fileParameter
	 *            the fileParameter to set
	 */
	public void setFileParameter(final String fileParameter) {
		this.fileParameter = fileParameter;
	}


	/**
	 * @return the formatParameter
	 */
	public String getFormatParameter() {
		return this.formatParameter;
	}


	/**
	 * @param formatParameter
	 *            the formatParameter to set
	 */
	public void setFormatParameter(final String formatParameter) {
		this.formatParameter = formatParameter;
	}


	/**
	 * @return the encoding
	 */
	public String getEncoding() {
		return this.encoding;
	}


	/**
	 * @param encoding
	 *            the encoding to set
	 */
	public void setEncoding(final String encoding) {
		this.encoding = encoding;
	}


	/**
	 * @return the exposeSubMediaUnits
	 */
	public boolean isExposeSubMediaUnits() {
		return this.exposeSubMediaUnits;
	}


	/**
	 * @param exposeSubMediaUnits
	 *            the exposeSubMediaUnits to set
	 */
	public void setExposeSubMediaUnits(final boolean exposeSubMediaUnits) {
		this.exposeSubMediaUnits = exposeSubMediaUnits;
	}


	/**
	 * @return the exposeThumbnails
	 */
	public boolean isExposeThumbnails() {
		return this.exposeThumbnails;
	}


	/**
	 * @param exposeThumbnails
	 *            the exposeThumbnails to set
	 */
	public void setExposeThumbnails(final boolean exposeThumbnails) {
		this.exposeThumbnails = exposeThumbnails;
	}

}
