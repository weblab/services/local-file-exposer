/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.exposer.config;

import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;



/**
 * This class is just a bean container for the configuration of LocalFileExposer service.
 *
 * @author ymombrun
 */
public class WebserverContextConfig {



	/**
	 * Whether the URL shall be annotated as Resource. If not it will be a literal statement.
	 */
	private boolean annotateAsUri;


	/**
	 * The String to be added at the beginning of the String to replace; in most of the case it's protocol://host:port/exposition_pattern/
	 */
	private String byString;


	/**
	 * It's possible to change the URI of the predicate to be written with the exposition URL.
	 */
	private String exposedAsUri;


	/**
	 * The number of character to be remove at the beginning of the String to replace; in most of the case it's the path to the whole folder that will be
	 * exposed.
	 */
	private int nbStartCharToReplace;


	/**
	 * The java method for URL encoding replaces spaces by '+'. But servers like Tomcat or Apache are using "%20" (in UTF-8 and ISO-8859-1 for instance) as replacement String.
	 */
	private String replacementForSpace;


	/**
	 * The URI of the service to add in the created annotation, or null if nothing has to be added.
	 */
	private String serviceUri;


	/**
	 * Sometimes the object of the statement containing the original source of the Document is a Resource (since an URL might be an URI); in this case this parameter has to be true.
	 */
	private boolean sourceIsResource;


	/**
	 * The predicate's URI of the statement that contains the original source of the Document to be modified.
	 */
	private String sourceUri;


	/**
	 * Whether or not to URL encode the slashes (and backslashes). In most of the case they do not have to.
	 */
	private boolean urlEncodeSlashes;


	/**
	 * When encoding a URL it's needed to use the encoding used by the server for URL encoding; otherwise some file might never be exposed (especially files containing accent in their name or other
	 * complex characters).
	 */
	private String urlEncoding;


	/**
	 * Populate the Bean with default values for exposedAsUri, urlEncoding, byString, replacementForSpace and sourceUri.
	 */
	public void populateWithDefaults() {
		if (this.exposedAsUri == null) {
			this.exposedAsUri = WebLabProcessing.IS_EXPOSED_AS;
		}
		if (this.urlEncoding == null) {
			this.urlEncoding = "ISO-8859-1";
		}
		if (this.byString == null) {
			this.byString = "";
		}
		if (this.replacementForSpace == null) {
			this.replacementForSpace = "%20";
		}
		if (this.sourceUri == null) {
			this.sourceUri = DublinCore.SOURCE_PROPERTY_NAME;
		}
	}


	/**
	 * If byString is null, set it to the empty string
	 *
	 * @return the byString
	 */
	public String getByString() {
		return this.byString;
	}


	/**
	 * @return the exposedAsUri
	 */
	public String getExposedAsUri() {
		return this.exposedAsUri;
	}


	/**
	 * @return the nbStartCharToReplace
	 */
	public int getNbStartCharToReplace() {
		return this.nbStartCharToReplace;
	}


	/**
	 * @return the replacementForSpace
	 */
	public String getReplacementForSpace() {
		return this.replacementForSpace;
	}


	/**
	 * @return the serviceUri
	 */
	public String getServiceUri() {
		return this.serviceUri;
	}


	/**
	 * if sourceUri is null
	 *
	 * @return the sourceUri
	 */
	public String getSourceUri() {
		return this.sourceUri;
	}


	/**
	 * If urlEncoding is null.
	 *
	 * @return the urlEncoding
	 */
	public String getUrlEncoding() {
		return this.urlEncoding;
	}


	/**
	 * @return the annotateAsUri
	 */
	public boolean isAnnotateAsUri() {
		return this.annotateAsUri;
	}


	/**
	 * @return the sourceIsResource
	 */
	public boolean isSourceIsResource() {
		return this.sourceIsResource;
	}


	/**
	 * @param annotateAsUri
	 *            the annotateAsUri to set
	 */
	public void setAnnotateAsUri(final boolean annotateAsUri) {
		this.annotateAsUri = annotateAsUri;
	}


	/**
	 * @param byString
	 *            the byString to set
	 */
	public void setByString(final String byString) {
		this.byString = byString;
	}


	/**
	 * @param exposedAsUri
	 *            the exposedAsUri to set
	 */
	public void setExposedAsUri(final String exposedAsUri) {
		this.exposedAsUri = exposedAsUri;
	}


	/**
	 * @param nbStartCharToReplace
	 *            the nbStartCharToReplace to set
	 */
	public void setNbStartCharToReplace(final int nbStartCharToReplace) {
		this.nbStartCharToReplace = nbStartCharToReplace;
	}


	/**
	 * @param replacementForSpace
	 *            the replacementForSpace to set
	 */
	public void setReplacementForSpace(final String replacementForSpace) {
		this.replacementForSpace = replacementForSpace;
	}


	/**
	 * @param serviceUri
	 *            the serviceUri to set
	 */
	public void setServiceUri(final String serviceUri) {
		this.serviceUri = serviceUri;
	}


	/**
	 * @param sourceIsResource
	 *            the sourceIsResource to set
	 */
	public void setSourceIsResource(final boolean sourceIsResource) {
		this.sourceIsResource = sourceIsResource;
	}


	/**
	 * @param sourceUri
	 *            the sourceUri to set
	 */
	public void setSourceUri(final String sourceUri) {
		this.sourceUri = sourceUri;
	}



	/**
	 * @param urlEncoding
	 *            the urlEncoding to set
	 */
	public void setUrlEncoding(final String urlEncoding) {
		this.urlEncoding = urlEncoding;
	}


	/**
	 * @return the urlEncodeSlashes
	 */
	public boolean isUrlEncodeSlashes() {
		return this.urlEncodeSlashes;
	}


	/**
	 * @param urlEncodeSlashes
	 *            the urlEncodeSlashes to set
	 */
	public void setUrlEncodeSlashes(final boolean urlEncodeSlashes) {
		this.urlEncodeSlashes = urlEncodeSlashes;
	}


}
