/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.exposer.enricher;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.annotator.AbstractAnnotator.Operator;
import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.service.exposer.config.WebserverContextConfig;
import org.ow2.weblab.service.exposer.constants.Constants;



/**
 * The purpose is to annotate documents in input with an "exposition URL" i.e. an URL that can be used in a web browser to access this document.
 *
 * This exposer only works when processed file are exposed using the server and have the same naming pattern than the read annotation on the document (in most of the case, we use dc:source).
 * Some parameter can be set using the deployment file.
 *
 * <ul>
 * <li><b><tt>sourceUri</tt></b>: The predicate's URI of the statement that contains the original source of the Document to be modified. Default value is
 * <code>http://purl.org/dc/elements/1.1/source</code>.</li>
 * <li><b><tt>sourceIsResource</tt></b>: Sometimes the object of the statement containing the original source of the Document is a Resource (since an URL might be an URI); in this case this parameter
 * has to be true. Default value is <code>false</code>.</li>
 * <li><b><tt>exposedAsUri</tt></b>: The predicate's URI of the statement that will contain the exposed URL of the Document. Default value is
 * <code>http://weblab.ow2.org/core/1.2/ontology/processing#isExposedAs</code>.</li>
 * <li><b><tt>nbStartCharToReplace</tt></b>: The number of character to be remove at the beginning of the String to replace; in most of the case it's the path to the whole folder that will be exposed.
 * Default value is <code>0</code>.</li>
 * <li><b><tt>byString</tt></b>: The String to be added at the beginning of the String to replace; in most of the case it's <tt>protocol://host:port/exposition_pattern/</tt>. Default value is
 * <code>""</code>.</li>
 * <li><b><tt>annotateAsUri</tt></b>: Whether the URL shall be annotated as Resource. If not it will be a literal statement. Default value is <code>false</code> .</li>
 * <li><b><tt>urlEncodeSlashes</tt></b>: Whether the slashes representing file hierarchy shall be encoded or not. Default value is <code>false</code>.</li>
 * <li><b><tt>urlEncoding</tt></b>: When encoding a URL it's needed to use the encoding used by the server for URL encoding; otherwise some file might never be exposed (especially files containing
 * accent in their name or other complex characters). Default value is <code>ISO-8859-1</code>.</li>
 * <li><b><tt>replacementForSpace</tt></b>: The java method for URL encoding replaces spaces by '+'. But servers like Tomcat or Apache are using "%20" (in UTF-8 and ISO-8859-1 for instance) as
 * replacement String. Default value is <code>%20</code>.</li>
 * <li><b><tt>serviceUri</tt></b>: The URI of the service to be added in the created annotation (with a produced by statement) or <code>null</code> if nothing should be added.</li>
 * </ul>
 *
 * @author ymombrun
 */
public class WebserverContextExposer implements DocumentEnricher {


	/**
	 * The configuration
	 */
	private final WebserverContextConfig conf;


	/**
	 * The logger
	 */
	private final Log logger;


	/**
	 * Initialise with default configuration.
	 */
	public WebserverContextExposer() {
		this(new WebserverContextConfig());
	}



	/**
	 * @param configuration
	 *            The configuration holder
	 */
	public WebserverContextExposer(final WebserverContextConfig configuration) {
		this.logger = LogFactory.getLog(this.getClass());
		configuration.populateWithDefaults();
		this.conf = configuration;
		this.logger.info("WebserverContextExposer successfully started.");
	}


	@Override
	public void enrichDocument(final Document document) throws UnexpectedException {
		// Get the source
		final Set<String> sources = new HashSet<>();
		if (this.conf.isSourceIsResource()) {
			for (final URI source : new BaseAnnotator(document).applyOperator(Operator.READ, null, URI.create(this.conf.getSourceUri()), URI.class, null).getValues()) {
				sources.add(source.toASCIIString());
			}
		} else {
			sources.addAll(new BaseAnnotator(document).applyOperator(Operator.READ, null, URI.create(this.conf.getSourceUri()), String.class, null).getValues());
		}

		final Iterator<String> sourceIt = sources.iterator();

		if (!sourceIt.hasNext()) {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'" + " Unable to properly process document. "+"No '" + this.conf.getSourceUri() + "' property found on resource '" + document.getUri() + "'. Nothing is done.");
			return;
		}

		final String originalSource = sourceIt.next();

		if (sourceIt.hasNext()) {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'" + " Unable to properly process document. "+"More than one value found for property '" + this.conf.getSourceUri() + "' in resource '" + document.getUri() + "'.");
			this.logger.trace("Values found were '" + sources + "'.");
			this.logger.trace("Only '" + originalSource + "' will be used.");
		}

		// Guess the exposition URL given the source

		String tempSource;
		final StringBuilder finalSource = new StringBuilder();
		if (this.conf.getNbStartCharToReplace() > 0) {
			if (originalSource.length() > this.conf.getNbStartCharToReplace()) {
				finalSource.append(this.conf.getByString());
				tempSource = originalSource.substring(this.conf.getNbStartCharToReplace());
			} else {
				this.logger.warn("'<"+Constants.SERVICE_NAME+">'" + " Unable to properly process document. "+"Source found (" + originalSource + ") is shorter than the number of characters to replace (" + String.valueOf(this.conf.getNbStartCharToReplace())
						+ "). Nothing is done.");
				return;
			}
		} else {
			this.logger.trace("nbStartCharToReplace is smaller that one; no replacement is done.");
			tempSource = originalSource;
		}

		tempSource = tempSource.replace('\\', '/');

		try {
			if (this.conf.isUrlEncodeSlashes()) {
				finalSource.append(URLEncoder.encode(tempSource, this.conf.getUrlEncoding()));
			} else {
				final String[] splitted = tempSource.split("/");
				for (final String element : splitted) {
					finalSource.append("/");
					finalSource.append(URLEncoder.encode(element, this.conf.getUrlEncoding()));
				}
			}
		} catch (final UnsupportedEncodingException uee) {
			final String message = this.conf.getUrlEncoding() + " is not supported. Check configuration file.";
			throw ExceptionFactory.createUnexpectedException("'<"+Constants.SERVICE_NAME+">'" + " Error processing document. "+message, uee);
		}

		final String exposedAs = finalSource.toString().replace("+", this.conf.getReplacementForSpace());

		// Annotate the document with the exposed as URL
		final Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(document);
		final WProcessingAnnotator wpa = new WProcessingAnnotator(URI.create(document.getUri()), annot);

		if (this.conf.isAnnotateAsUri()) {
			wpa.applyOperator(Operator.WRITE, "exp", URI.create(this.conf.getExposedAsUri()), URI.class, URI.create(exposedAs));
		} else {
			wpa.applyOperator(Operator.WRITE, "exp", URI.create(this.conf.getExposedAsUri()), String.class, exposedAs);
		}
		if (this.conf.getServiceUri() != null) {
			wpa.startInnerAnnotatorOn(URI.create(annot.getUri()));
			wpa.writeProducedBy(URI.create(this.conf.getServiceUri()));
		}

	}

}
