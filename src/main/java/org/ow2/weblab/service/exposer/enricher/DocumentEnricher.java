/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.exposer.enricher;

import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.services.UnexpectedException;



/**
 * The interface that defines the enrich method in charge of annotating the document based on some of its internal features in order to add an isExposedAs URL.
 *
 * @author ymombrun
 */
public interface DocumentEnricher {


	/**
	 * Method in charge of the enrichment of the provided document by an annotation enabling its exposition.
	 *
	 * @param document
	 *            The document to be enriched
	 * @throws UnexpectedException If something wrong occurs
	 */
	public void enrichDocument(final Document document) throws UnexpectedException;

}
