/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.exposer.enricher;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.HashSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.rdf.Value;
import org.ow2.weblab.service.exposer.config.RestServiceConfig;
import org.ow2.weblab.service.exposer.constants.Constants;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.purl.dc.terms.DCTermsAnnotator;


/**
 * This class is in charge of generating a custom URL request based on the input document. It creates an annotation with query parameters for file name and file format.
 *
 * @author ymombrun
 */
public class RestServiceExposer implements DocumentEnricher {


	public static final URI THUMBNAIL = URI.create("http://weblab.ow2.org/core/1.2/ontology/processing#Thumbnail");


	/**
	 * The logger
	 */
	private final Log logger;


	/**
	 * The configuration holder
	 */
	private final RestServiceConfig conf;


	/**
	 * @param configuration
	 *            The configuration holder
	 */
	public RestServiceExposer(final RestServiceConfig configuration) {
		this.conf = configuration;
		configuration.populateWithDefaults();
		this.logger = LogFactory.getLog(this.getClass());
	}


	/**
	 * Constructor with default value
	 */
	public RestServiceExposer() {
		this(new RestServiceConfig());
	}


	@Override
	public void enrichDocument(final Document document) throws UnexpectedException {
		this.exposeWithAnyContent(document);

		if (this.conf.isExposeSubMediaUnits()) {
			for (final MediaUnit mu : document.getMediaUnit()) {
				this.exposeWithAnyContent(mu);
			}
		}
	}


	/**
	 * @param mediaUnit
	 *            The mediaUnit on which the native/normalised content should be read
	 * @throws UnexpectedException
	 *             If an encoding exception occurred when creating the URL
	 */
	private void exposeWithAnyContent(final MediaUnit mediaUnit) throws UnexpectedException {
		final WProcessingAnnotator wpa = new WProcessingAnnotator(mediaUnit);
		final Value<URI> contentsNativeFirst = wpa.readNativeContent();
		contentsNativeFirst.aggregate(wpa.readNormalisedContent());
		if (contentsNativeFirst.hasValue()) {
			final URI mainContentUri = contentsNativeFirst.firstTypedValue();
			final String mainExposedAs = this.generateExposedAsValue(mediaUnit, mainContentUri);
			wpa.writeExposedAs(mainExposedAs);

			for (final URI contentUri : new HashSet<>(contentsNativeFirst.getValues())) {
				final String exposedAs = this.generateExposedAsValue(mediaUnit, contentUri);
				wpa.startInnerAnnotatorOn(contentUri);
				wpa.writeExposedAs(exposedAs);
				wpa.endInnerAnnotator();
			}

			if (this.conf.isExposeThumbnails()) {
				for (final URI contentUri : new HashSet<>(contentsNativeFirst.getValues())) {
					wpa.startInnerAnnotatorOn(contentUri);
					if (wpa.readType().getValues().contains(RestServiceExposer.THUMBNAIL)) {
						wpa.endInnerAnnotator(); //
						final String exposedAsThumbnail = this.generateExposedAsValue(mediaUnit, contentUri);
						wpa.writeExposedAsThumbnail(exposedAsThumbnail);
						wpa.startInnerAnnotatorOn(contentUri);
						wpa.writeExposedAsThumbnail(exposedAsThumbnail);
					}
					wpa.endInnerAnnotator();
				}
			}

		}

	}


	/**
	 * @param mediaUnit
	 *            The media unit to be exposed
	 * @param wpa
	 *            The WProcessingAnnotator opened on that media unit
	 * @param contentUri
	 *            The native content URI
	 * @throws UnexpectedException
	 *             If an error occurred with the encoding. This should never happen.
	 * @return The String to be annotated as isExposedAs or isExposedAsThumbnail
	 */
	private String generateExposedAsValue(final MediaUnit mediaUnit, final URI contentUri) throws UnexpectedException {
		final String filePath;
		if (contentUri.isAbsolute() && contentUri.isOpaque()) {
			filePath = contentUri.getSchemeSpecificPart();
		} else if (contentUri.isAbsolute()) {
			filePath = new File(contentUri).getName();
		} else {
			final String message = contentUri.toASCIIString() + " is not absolute.";
			this.logger.error("'<"+Constants.SERVICE_NAME+">'" + " Error processing document. "+message);
			throw ExceptionFactory.createUnexpectedException(message);
		}

		final Value<String> fullFormat = new DCTermsAnnotator(mediaUnit).readFormatOf();
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(mediaUnit);
		final Value<String> docFormat = dca.readFormat();
		dca.startInnerAnnotatorOn(contentUri);
		final Value<String> contentFormat = dca.readFormat();

		final StringBuilder sb = new StringBuilder();
		try {
			sb.append(this.conf.getPrefix());
			sb.append("?");
			sb.append(this.conf.getFileParameter());
			sb.append("=");
			sb.append(URLEncoder.encode(filePath, this.conf.getEncoding()));
			if (fullFormat.hasValue()) {
				sb.append("&");
				sb.append(this.conf.getFormatParameter());
				sb.append("=");
				sb.append(URLEncoder.encode(fullFormat.firstTypedValue(), this.conf.getEncoding()));
			} else if (contentFormat.hasValue()) {
				sb.append("&");
				sb.append(this.conf.getFormatParameter());
				sb.append("=");
				sb.append(URLEncoder.encode(contentFormat.firstTypedValue(), this.conf.getEncoding()));
			} else if (docFormat.hasValue()) {
				sb.append("&");
				sb.append(this.conf.getFormatParameter());
				sb.append("=");
				sb.append(URLEncoder.encode(docFormat.firstTypedValue(), this.conf.getEncoding()));
			}

		} catch (final UnsupportedEncodingException uee) {
			final String message = this.conf.getEncoding() + " is not supported. Check configuration file.";
			throw ExceptionFactory.createUnexpectedException("'<"+Constants.SERVICE_NAME+">'" + " Error processing document "+message, uee);
		}
		return sb.toString();
	}

}
