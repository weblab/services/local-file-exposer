/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.exposer;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.annotator.AbstractAnnotator.Operator;
import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.service.exposer.config.ExposerConfigBean;

/**
 * This service is used in conjunction with an Apache Tomcat or Apache server.
 *
 * The purpose is to annotate documents in input with an "exposition URL" i.e. an URL that can be used in a web browser to access this document.
 *
 * This service only works when processed file are exposed using the server and have the same naming pattern than the read annotation on the document (in most of the case, we use dc:source).
 * Some parameter can be set using the deployment file.
 *
 * <ul>
 * <li><b><tt>sourceUri</tt></b>: The predicate's URI of the statement that contains the original source of the Document to be modified. Default value is
 * <code>http://purl.org/dc/elements/1.1/source</code>.</li>
 * <li><b><tt>sourceIsResource</tt></b>: Sometimes the object of the statement containing the original source of the Document is a Resource (since an URL might be an URI); in this case this parameter
 * has to be true. Default value is <code>false</code>.</li>
 * <li><b><tt>exposedAsUri</tt></b>: The predicate's URI of the statement that will contain the exposed URL of the Document. Default value is
 * <code>http://weblab.ow2.org/core/1.2/ontology/processing#isExposedAs</code>.</li>
 * <li><b><tt>nbStartCharToReplace</tt></b>: The number of character to be remove at the beginning of the String to replace; in most of the case it's the path to the whole folder that will be exposed.
 * Default value is <code>0</code>.</li>
 * <li><b><tt>byString</tt></b>: The String to be added at the beginning of the String to replace; in most of the case it's <tt>protocol://host:port/exposition_pattern/</tt>. Default value is
 * <code>""</code>.</li>
 * <li><b><tt>annotateAsUri</tt></b>: Whether the URL shall be annotated as Resource. If not it will be a literal statement. Default value is <code>false</code> .</li>
 * <li><b><tt>urlEncodeBetweenSlashes</tt></b>: Whether the URL shall be annotated as Resource. If not it will be a literal statement. Default value is <code>false</code>.</li>
 * <li><b><tt>urlEncoding</tt></b>: When encoding a URL it's needed to use the encoding used by the server for URL encoding; otherwise some file might never be exposed (especially files containing
 * accent in their name or other complex characters). Default value is <code>ISO-8859-1</code>.</li>
 * <li><b><tt>replacementForSpace</tt></b>: The java method for URL encoding replaces spaces by '+'. But servers like Tomcat or Apache are using "%20" (in UTF-8 and ISO-8859-1 for instance) as
 * replacement String. Default value is <code>%20</code>.</li>
 * <li><b><tt>serviceUri</tt></b>: The URI of the service to be added in the created annotation (with a produced by statement) or <code>null</code> if nothing should be added.</li>
 * </ul>
 *
 * @author ymombrun
 * @deprecated You'd rather to use the new class {@link CustomFileExposer}
 */
@Deprecated
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class LocalFileExposer implements Analyser {


	private final Log logger;


	/**
	 * The configuration
	 */
	private final ExposerConfigBean conf;


	/**
	 * Constructors
	 *
	 * @param configuration
	 *            The configuration holder
	 */
	public LocalFileExposer(final ExposerConfigBean configuration) {
		this.logger = LogFactory.getLog(this.getClass());
		this.conf = configuration;
		this.logger.info("LocalFileExposer successfully started.");
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws UnexpectedException, InvalidParameterException {
		this.logger.debug("Early start of process method.");
		final Document doc = this.checkArgs(args);

		this.logger.info("Start processing document " + doc.getUri() + ".");

		// Get the sources
		final Set<String> sources = new HashSet<>();
		if (this.conf.isSourceIsResource()) {
			for (final URI source : new BaseAnnotator(doc).applyOperator(Operator.READ, null, URI.create(this.conf.getSourceUri()), URI.class, null).getValues()) {
				sources.add(source.toASCIIString());
			}
		} else {
			sources.addAll(new BaseAnnotator(doc).applyOperator(Operator.READ, null, URI.create(this.conf.getSourceUri()), String.class, null).getValues());
		}

		final Iterator<String> sourceIt = sources.iterator();

		if (!sourceIt.hasNext()) {
			this.logger.warn("No '" + this.conf.getSourceUri() + "' property found in resource '" + doc.getUri() + "'. Nothing is done.");
			final ProcessReturn pr = new ProcessReturn();
			pr.setResource(doc);
			return pr;
		}

		final String originalSource = sourceIt.next();

		if (sourceIt.hasNext()) {
			this.logger.warn("More than one value found for property '" + this.conf.getSourceUri() + "' in resource '" + doc.getUri() + "'.");
			this.logger.debug("Values found were '" + sources + "'.");
			this.logger.debug("'" + originalSource + "' will be used.");
		}

		// Guess the exposition URL given the source

		String tempSource;
		final StringBuilder finalSource = new StringBuilder();
		if (this.conf.getNbStartCharToReplace() > 1) {
			if (originalSource.length() > this.conf.getNbStartCharToReplace()) {
				finalSource.append(this.conf.getByString());
				tempSource = originalSource.substring(this.conf.getNbStartCharToReplace());
			} else {

				this.logger.warn("Source found (" + originalSource + ") is smaller than the number of character to replace (" + String.valueOf(this.conf.getNbStartCharToReplace())
						+ "). Nothing is done.");
				final ProcessReturn pr = new ProcessReturn();
				pr.setResource(doc);
				return pr;
			}
		} else {
			this.logger.debug("nbStartCharToReplace is smaller that one; no replacement is done.");
			tempSource = originalSource;
		}

		tempSource = tempSource.replace('\\', '/');

		if (this.conf.isUrlEncodeBetweenSlashes()) {
			try {
				final String[] splitted = tempSource.split("/");
				for (final String element : splitted) {
					finalSource.append("/");
					finalSource.append(URLEncoder.encode(element, this.conf.getUrlEncoding()));
				}
			} catch (final UnsupportedEncodingException uee) {
				this.logger.fatal(this.conf.getUrlEncoding() + " is not supported. Check configuration file.", uee);
				throw ExceptionFactory.createUnexpectedException(this.conf.getUrlEncoding() + " is not supported. Check configuration file.", uee);
			}
		} else {
			if (this.conf.isUrlEncodeSlashes()) {
				try {
					finalSource.append(URLEncoder.encode(tempSource, this.conf.getUrlEncoding()));
				} catch (final UnsupportedEncodingException uee) {
					this.logger.fatal(this.conf.getUrlEncoding() + " is not supported. Check configuration file.", uee);
					throw ExceptionFactory.createUnexpectedException(this.conf.getUrlEncoding() + " is not supported. Check configuration file.", uee);
				}
			} else {
				finalSource.append(tempSource);
			}
		}

		final String exposedAs = finalSource.toString().replace("+", this.conf.getReplacementForSpace());


		// Annotate the document with the exposed as URL
		final Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(doc);
		final WProcessingAnnotator wpa = new WProcessingAnnotator(URI.create(doc.getUri()), annot);

		if (this.conf.isAnnotateAsUri()) {
			wpa.applyOperator(Operator.WRITE, "exp", URI.create(this.conf.getExposedAsUri()), URI.class, URI.create(exposedAs));
		} else {
			wpa.applyOperator(Operator.WRITE, "exp", URI.create(this.conf.getExposedAsUri()), String.class, exposedAs);
		}
		if (this.conf.getServiceUri() != null) {
			wpa.startInnerAnnotatorOn(URI.create(annot.getUri()));
			wpa.writeProducedBy(URI.create(this.conf.getServiceUri()));
		}

		final ProcessReturn pr = new ProcessReturn();
		pr.setResource(doc);
		return pr;
	}


	/**
	 * @param args
	 *            The ProcessArgs to check validity
	 * @return The document that must contain the resource in ProcessArgs
	 */
	private Document checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String message = "ProcessArgs is null";
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		final Resource res = args.getResource();
		if (res == null) {
			final String message = "Resource is null";
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		if (!(res instanceof Document)) {
			final String message = "Resource was not a Document but a: " + args.getResource().getClass().getName() + ".";
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		if (!args.getResource().isSetAnnotation()) {
			final String message = "Document does not contains any Annotation.";
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		return (Document) args.getResource();
	}

}
