/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.exposer.config;


/**
 * This class is just a bean container for the configuration of LocalFileExposer service.
 *
 * @author IPCC WebLab Team
 */
@Deprecated
public class ExposerConfigBean extends WebserverContextConfig {


	// This class is just maintained for compatibility purposes. However the LocalFileExposer that use it is deprecated too.


	/**
	 * Whether or not to URL encode between the slashes (and backslashes). It's needed in most of the case.
	 */
	private boolean urlEncodeBetweenSlashes;


	/**
	 * @return the urlEncodeBetweenSlashes
	 */
	public boolean isUrlEncodeBetweenSlashes() {
		return this.urlEncodeBetweenSlashes;
	}


	/**
	 * @param urlEncodeBetweenSlashes
	 *            the urlEncodeBetweenSlashes to set
	 */
	public void setUrlEncodeBetweenSlashes(final boolean urlEncodeBetweenSlashes) {
		this.urlEncodeBetweenSlashes = urlEncodeBetweenSlashes;
	}

}
