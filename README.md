# Local File Exposer service

This project is a Maven based Java project of a Web Service.
This WebLab WebService is in charge of annotating Resource and Content with wlp:isExposedAs properties that could be used later on the UI for downloading the content (original or converted).


# Build status

## Master
[![build status](https://gitlab.ow2.org/weblab/services/local-file-exposer/badges/master/build.svg)](https://gitlab.ow2.org/weblab/services/local-file-exposer/commits/master)
## Develop
[![build status](https://gitlab.ow2.org/weblab/services/local-file-exposer/badges/develop/build.svg)](https://gitlab.ow2.org/weblab/services/local-file-exposer/commits/develop)


# How to build it

In order to be build it, you need to have access to the Maven dependencies it is using. Most of the dependencies are in the central repository and thus does not implies specific configuration.
However, the WebLab Core dependencies are not yet included in the Maven central repository but in a dedicated one that we manage ourselves.
Thus you may have to add the repositories that are listed in the settings.xml. 
